"""
 ------------------------------------------------------------------------------
Unit Test for Detector Beckhoff PLC Utility. At the moment is only monitoring
one pressure and one temperature analog channels at a EL3052 terminal.
 ------------------------------------------------------------------------------
(c) 2019 European Spallation Source, Lund
 ------------------------------------------------------------------------------
 author: douglas.bezerra.beniz@esss.se
  ------------------------------------------------------------------------------
"""
import sys, time
import unittest
import epics

from enum import Enum

# -----------------------------------------------------------------------------
# --------------------------------- WARNING! ----------------------------------
# Take care with this tests execution; it is pointing to a real PLC;
# -----------------------------------------------------------------------------
# det_plcutil IP: 172.30.244.21
# -----------------------------------------------------------------------------
PVS_PREFIX = "Utg-VIP:Det-Util-01"

# -----------------------------------------------------------------------------
# EPICS PVs
# -----------------------------------------------------------------------------
PVS = {
    "press01_val":  "%s:pressure_01.VAL"  % PVS_PREFIX,
    "press01_stat": "%s:pressure_01.STAT" % PVS_PREFIX,
    "press01_hihi": "%s:pressure_01.HIHI" % PVS_PREFIX,
    "press01_high": "%s:pressure_01.HIGH" % PVS_PREFIX,
    "press01_low":  "%s:pressure_01.LOW"  % PVS_PREFIX,
    "press01_lolo": "%s:pressure_01.LOLO" % PVS_PREFIX,
    "temp01_val":  "%s:temperature_01.VAL"  % PVS_PREFIX,
    "temp01_stat": "%s:temperature_01.STAT" % PVS_PREFIX,
    "temp01_hihi": "%s:temperature_01.HIHI" % PVS_PREFIX,
    "temp01_high": "%s:temperature_01.HIGH" % PVS_PREFIX,
    "temp01_low":  "%s:temperature_01.LOW"  % PVS_PREFIX,
    "temp01_lolo": "%s:temperature_01.LOLO" % PVS_PREFIX,
}

# -----------------------------------------------------------------------------
# Predefined values to add/subtract from the measured value when forcing limits;
# -----------------------------------------------------------------------------
MIN_VAR_VALUE = 0.5
MAX_VAR_VALUE = 1.0

"""
Class to define enumeration with all EPICS supported alarm status.
"""
class epicsAlarms(Enum):
    NONE            = 0
    READ            = 1
    WRITE           = 2
    HIHI            = 3
    HIGH            = 4
    LOLO            = 5
    LOW             = 6
    STATE           = 7
    COS             = 8
    COMM            = 9
    TIMEOUT         = 10
    HWLIMIT         = 11
    CALC            = 12
    SCAN            = 13
    LINK            = 14
    SOFT            = 15
    BAD             = 16
    UDF             = 17
    DISABLE         = 18
    SIMM            = 19
    READ_ACCESS     = 20
    WRITE_ACCESS    = 21

"""
Definition of the main test cases
"""
class MyCase(unittest.TestCase):
    def __init__(self, *args, **kwargs):
        super(MyCase, self).__init__(*args, **kwargs)
        self.previousHihi = None
        self.previousHigh = None
        self.previousLow  = None
        self.previousLolo = None

    def setUp(self):
        self.verificationErrors = []

    def tearDown(self):
        self.assertEqual([], self.verificationErrors)

    # ---------------------------------------------------------------------
    # ------------------------ Pressure test cases ------------------------
    # Because we're reading a real device, and the intention was not to change
    #    real-time measurements, limit setpoints are being modified instead.
    # ---------------------------------------------------------------------
    """
    Verify if HIHI flag is correctly assigned when a limit is reached;
    """
    def test_pressure_hihi_alarm(self):
        # verify if HIHI flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["press01_val"])
        # then, calculates the HIGH and HIHI setpoints...
        newHihi = curVal - MIN_VAR_VALUE
        newHigh = curVal - MAX_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new HIGH and HIHI limits
        epics.caput(PVS["press01_hihi"], newHihi)
        epics.caput(PVS["press01_high"], newHigh)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        try:
            self.assertEqual(
                int(epics.caget(PVS["press01_stat"])),
                epicsAlarms.HIHI.value,
                "HIHI alarm has not been set as expected;")
        except AssertionError as err:
            self.verificationErrors.append(str(err))
        finally:
            # restore salved limits...
            self._restore_previous_limits("press")


    """
    Verify if HIGH flag is correctly assigned when a limit is reached;
    """
    def test_pressure_high_alarm(self):
        # verify if HIGH flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["press01_val"])
        # then, calculate the HIGH and HIHI setpoints...
        newHihi = curVal + MAX_VAR_VALUE
        newHigh = curVal - MIN_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new HIGH and HIHI limits
        epics.caput(PVS["press01_hihi"], newHihi)
        epics.caput(PVS["press01_high"], newHigh)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["press01_stat"])), epicsAlarms.HIGH.value,"HIGH alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("press")


    """
    Verify if LOW flag is correctly assigned when a limit is reached;
    """
    def test_pressure_low__alarm(self):
        # verify if LOW flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["press01_val"])
        # then, calculate the LOW and LOLO setpoints...
        newLow  = curVal + MIN_VAR_VALUE
        newLolo = curVal - MAX_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new LOW and LOLO limits
        epics.caput(PVS["press01_low"], newLow)
        epics.caput(PVS["press01_lolo"], newLolo)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["press01_stat"])), epicsAlarms.LOW.value,"LOW alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("press")


    """
    Verify if LOLO flag is correctly assigned when a limit is reached;
    """
    def test_pressure_lolo_alarm(self):
        # verify if LOW flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["press01_val"])
        # then, calculate the LOW and LOLO setpoints...
        newLow  = curVal + MAX_VAR_VALUE
        newLolo = curVal + MIN_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new LOW and LOLO limits
        epics.caput(PVS["press01_low"], newLow)
        epics.caput(PVS["press01_lolo"], newLolo)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["press01_stat"])), epicsAlarms.LOLO.value,"LOLO alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("press")


    # ---------------------------------------------------------------------
    # ---------------------- Temperature test cases -----------------------
    # Because we're reading a real device, and the intention was not to change
    #    real-time measurements, limit setpoints are being modified instead.
    # ---------------------------------------------------------------------
    """
    Verify if HIHI flag is correctly assigned when a limit is reached;
    """
    def test_temperature_hihi_alarm(self):
        # verify if HIHI flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["temp01_val"])
        # then, calculates the HIGH and HIHI setpoints...
        newHihi = curVal - MIN_VAR_VALUE
        newHigh = curVal - MAX_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new HIGH and HIHI limits
        epics.caput(PVS["temp01_hihi"], newHihi)
        epics.caput(PVS["temp01_high"], newHigh)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["temp01_stat"])), epicsAlarms.HIHI.value,"HIHI alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("temp")


    """
    Verify if HIGH flag is correctly assigned when a limit is reached;
    """
    def test_temperature_high_alarm(self):
        # verify if HIGH flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["temp01_val"])
        # then, calculate the HIGH and HIHI setpoints...
        newHihi = curVal + MAX_VAR_VALUE
        newHigh = curVal - MIN_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new HIGH and HIHI limits
        epics.caput(PVS["temp01_hihi"], newHihi)
        epics.caput(PVS["temp01_high"], newHigh)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["temp01_stat"])), epicsAlarms.HIGH.value,"HIGH alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("temp")


    """
    Verify if LOW flag is correctly assigned when a limit is reached;
    """
    def test_temperature_low__alarm(self):
        # verify if LOW flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["temp01_val"])
        # then, calculate the LOW and LOLO setpoints...
        newLow  = curVal + MIN_VAR_VALUE
        newLolo = curVal - MAX_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new LOW and LOLO limits
        epics.caput(PVS["temp01_low"], newLow)
        epics.caput(PVS["temp01_lolo"], newLolo)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["temp01_stat"])), epicsAlarms.LOW.value,"LOW alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("temp")


    """
    Verify if LOLO flag is correctly assigned when a limit is reached;
    """
    def test_temperature_lolo_alarm(self):
        # verify if LOW flag is correctly assigned when a limit is reached;
        curVal = epics.caget(PVS["temp01_val"])
        # then, calculate the LOW and LOLO setpoints...
        newLow  = curVal + MAX_VAR_VALUE
        newLolo = curVal + MIN_VAR_VALUE
        # at first, save all the previous limits...
        self._save_previous_limits("press")
        # set new LOW and LOLO limits
        epics.caput(PVS["temp01_low"], newLow)
        epics.caput(PVS["temp01_lolo"], newLolo)
        # wait a while
        time.sleep(0.5)
        # check if the correct alarm has been set...
        self.assertEqual(int(epics.caget(PVS["temp01_stat"])), epicsAlarms.LOLO.value,"LOLO alarm has not been set as expected;")
        # restore salved limits...
        self._restore_previous_limits("temp")

    """
    Utility function to save all the current limits (HIHI, HIGH, LOW and LOLO)
    """
    def _save_previous_limits(self, device):
        if device == "press":
            self.previousHihi = epics.caget(PVS["press01_hihi"])
            self.previousHigh = epics.caget(PVS["press01_high"])
            self.previousLow  = epics.caget(PVS["press01_low"])
            self.previousLolo = epics.caget(PVS["press01_lolo"])
        elif device == "temp":
            self.previousHihi = epics.caget(PVS["temp01_hihi"])
            self.previousHigh = epics.caget(PVS["temp01_high"])
            self.previousLow  = epics.caget(PVS["temp01_low"])
            self.previousLolo = epics.caget(PVS["temp01_lolo"])

    """
    Utility function to restore all the current limits (HIHI, HIGH, LOW and LOLO)
    """
    def _restore_previous_limits(self, device):
        if device == "press":
            epics.caput(PVS["press01_hihi"], self.previousHihi)
            epics.caput(PVS["press01_high"], self.previousHigh)
            epics.caput(PVS["press01_low"],  self.previousLow)
            epics.caput(PVS["press01_lolo"], self.previousLolo)
        elif device == "temp":
            epics.caput(PVS["temp01_hihi"], self.previousHihi)
            epics.caput(PVS["temp01_high"], self.previousHigh)
            epics.caput(PVS["temp01_low"],  self.previousLow)
            epics.caput(PVS["temp01_lolo"], self.previousLolo)
        #time.sleep(2)

"""
Definition of the main suite
"""
class MySuite1(unittest.TestSuite):
    def suite(self):
        suite = unittest.TestSuite()
        # ---------------------------------------------------------------------
        # Pressure test cases
        # ---------------------------------------------------------------------
        suite.addTest(MyCase("test_pressure_hihi_alarm"))
        time.sleep(0.5)
        suite.addTest(MyCase("test_pressure_high_alarm"))
        time.sleep(0.5)
        suite.addTest(MyCase("test_pressure_low__alarm"))
        time.sleep(0.5)
        suite.addTest(MyCase("test_pressure_lolo_alarm"))
        time.sleep(0.5)
        # ---------------------------------------------------------------------
        # Pressure test cases
        # ---------------------------------------------------------------------
        suite.addTest(MyCase("test_temperature_hihi_alarm"))
        time.sleep(0.5)
        suite.addTest(MyCase("test_temperature_high_alarm"))
        time.sleep(0.5)
        suite.addTest(MyCase("test_temperature_low__alarm"))
        time.sleep(0.5)
        suite.addTest(MyCase("test_temperature_lolo_alarm"))
        time.sleep(0.5)

        return suite

if __name__ == "__main__":
    unittest.TestLoader.sortTestMethodsUsing = 1
    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(MySuite1().suite())
