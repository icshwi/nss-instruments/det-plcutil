# -----------------------------------------------------------------------------
# Detector group PLC to monitor pressure, temperature and mass flow
# -----------------------------------------------------------------------------
# Hostname:  MB_PLC
# IP:        172.30.244.96
# -----------------------------------------------------------------------------
# Author: tamas.kerenyi@esss.se
# -----------------------------------------------------------------------------

##################
# STATUS BLOCK
##################
define_status_block()
add_analog("pressure_01",     "REAL",  PV_DESC="Pressure sensor 01",     PV_EGU="Bara",  PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("temperature_01",  "REAL",  PV_DESC="Temperature sensor 01",  PV_EGU="C",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)

add_analog("al_ctlr_gasindex",  "INT",  PV_DESC="Alicat controller gas index",  PV_EGU="",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_ctlr_pressure",  "REAL",  PV_DESC="Alicat controller pressure",  PV_EGU="Bara",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_ctlr_temperature",  "REAL",  PV_DESC="Alicat controller flow temperature",  PV_EGU="C",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_ctlr_ccm",  "REAL",  PV_DESC="Alicat controller volumetric flow",  PV_EGU="CCM",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_ctlr_sccm",  "REAL",  PV_DESC="Alicat controller mass flow",  PV_EGU="SCCM",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_ctlr_sp_rbv",  "REAL",  PV_DESC="Alicat controller mass flow setpoint RBV",  PV_EGU="",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_ctlr_ID_rbv",  "INT",  PV_DESC="Alicat controller ID RBV",  PV_EGU="",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)

add_analog("al_mt_gasindex",  "INT",  PV_DESC="Alicat meter gas index",  PV_EGU="",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_mt_pressure",  "REAL",  PV_DESC="Alicat meter pressure",  PV_EGU="Bara",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_mt_temperature",  "REAL",  PV_DESC="Alicat meter flow temperature",  PV_EGU="C",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_mt_ccm",  "REAL",  PV_DESC="Alicat meter volumetric flow",  PV_EGU="CCM",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_mt_sccm",  "REAL",  PV_DESC="Alicat meter mass flow",  PV_EGU="SCCM",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)
add_analog("al_mt_ID_rbv",  "INT",  PV_DESC="Alicat meter ID RBV",  PV_EGU="",    PV_HHSV=2,   PV_HSV=1,   PV_LSV=1,   PV_LLSV=2)

add_major_alarm("DET-PLCUtil pressure OK", "DET-PLCUtil pressure alarm", PV_NAME="pressure_01.STAT", PV_DESC="Limits for press has been exceeded", PV_ONAM="Pressure alarm")
add_major_alarm("DET-PLCUtil temperature OK", "DET-PLCUtil temperature alarm", PV_NAME="temperature_01.STAT", PV_DESC="Limits for temp has been exceeded", PV_ONAM="Temperature alarm" )

add_major_alarm("Alicat Controller pressure OK", "alicat controller pressure alarm", PV_NAME="al_ctlr_pressure.STAT", PV_DESC="Limits for press has been exceeded", PV_ONAM="Pressure alarm" )
add_major_alarm("Alicat Controller temperature OK", "alicat controller temperature alarm", PV_NAME="al_ctlr_temperature.STAT", PV_DESC="Limits for temp has been exceeded", PV_ONAM="Temperature alarm")
add_major_alarm("Alicat Controller flow OK", "alicat controller flow alarm", PV_NAME="al_ctlr_sccm.STAT", PV_DESC="Limits for flow has been exceeded", PV_ONAM="Mass flow alarm")

add_major_alarm("Alicat Meter pressure OK", "alicat meter pressure alarm", PV_NAME="al_mt_pressure.STAT", PV_DESC="Limits for press has been exceeded", PV_ONAM="Pressure alarm")
add_major_alarm("Alicat Meter temperature OK", "alicat meter temperature alarm", PV_NAME="al_mt_temperature.STAT", PV_DESC="Limits for temp has been exceeded", PV_ONAM="Temperature alarm")
add_major_alarm("Alicat Meter flow OK", "alicat meterer flow alarm", PV_NAME="al_mt_sccm.STAT", PV_DESC="Limits for flow has been exceeded", PV_ONAM="Mass flow alarm")

##################
# COMMAND BLOCK
##################
define_command_block()
add_digital("toggle_bit", PV_DESC="Start function")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()
add_analog("al_ctlr_sp",  "REAL",  PV_DESC="Alicat controller setpoint",     PV_EGU="")

