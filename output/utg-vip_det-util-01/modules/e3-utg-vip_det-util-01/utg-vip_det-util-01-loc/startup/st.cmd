# -----------------------------------------------------------------------------
# EPICS siteMods
# -----------------------------------------------------------------------------
require utg-vip_det-util-01, plcfactory

# -----------------------------------------------------------------------------
# Utgard-Lab
# -----------------------------------------------------------------------------
epicsEnvSet(IPADDR,       "172.30.244.29")
epicsEnvSet(RECVTIMEOUT,  3000)
epicsEnvSet(SAVEFILE_DIR, "/var/log/utg-vip_det-util-01")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
iocshLoad("$(utg-vip_det-util-01_DIR)utg-vip_det-util-01.iocsh", "IPADDR=$(IPADDR), RECVTIMEOUT=$(RECVTIMEOUT), SAVEFILE_DIR=$(SAVEFILE_DIR)")

iocInit