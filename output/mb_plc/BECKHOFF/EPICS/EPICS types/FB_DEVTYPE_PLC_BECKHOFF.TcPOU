<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject 
Version="1.1.0.1" ProductVersion="3.1.4022.10">
<POU 
Name="FB_DEVTYPE_PLC_BECKHOFF" Id="{5bb54db1-6fe3-4b17-2b0f-000000000003}" SpecialFunc="None">
<Declaration>
<![CDATA[
FUNCTION_BLOCK FB_DEVTYPE_PLC_BECKHOFF
VAR_INPUT
      nOffsetStatus	:INT;			//Offset for status variables
	  nOffsetCmd   :INT;			//Offset for command variables
      nOffsetPar   :INT;			//Offset for parameter variables
      pressure_01  :REAL;        //EPICS Status variable: pressure_01
      temperature_01  :REAL;        //EPICS Status variable: temperature_01
      al_ctlr_gasindex  :INT;        //EPICS Status variable: al_ctlr_gasindex
      al_ctlr_pressure  :REAL;        //EPICS Status variable: al_ctlr_pressure
      al_ctlr_temperature  :REAL;        //EPICS Status variable: al_ctlr_temperature
      al_ctlr_ccm  :REAL;        //EPICS Status variable: al_ctlr_ccm
      al_ctlr_sccm  :REAL;        //EPICS Status variable: al_ctlr_sccm
      al_ctlr_sp_rbv  :REAL;        //EPICS Status variable: al_ctlr_sp_rbv
      al_ctlr_ID_rbv  :INT;        //EPICS Status variable: al_ctlr_ID_rbv
      al_mt_gasindex  :INT;        //EPICS Status variable: al_mt_gasindex
      al_mt_pressure  :REAL;        //EPICS Status variable: al_mt_pressure
      al_mt_temperature  :REAL;        //EPICS Status variable: al_mt_temperature
      al_mt_ccm  :REAL;        //EPICS Status variable: al_mt_ccm
      al_mt_sccm  :REAL;        //EPICS Status variable: al_mt_sccm
      al_mt_ID_rbv  :INT;        //EPICS Status variable: al_mt_ID_rbv
      DET-PLCUtil pressure OK  :BOOL;        //EPICS Status variable: pressure_01.STAT
      DET-PLCUtil temperature OK  :BOOL;        //EPICS Status variable: temperature_01.STAT
      Alicat Controller pressure OK  :BOOL;        //EPICS Status variable: al_ctlr_pressure.STAT
      Alicat Controller temperature OK  :BOOL;        //EPICS Status variable: al_ctlr_temperature.STAT
      Alicat Controller flow OK  :BOOL;        //EPICS Status variable: al_ctlr_sccm.STAT
      Alicat Meter pressure OK  :BOOL;        //EPICS Status variable: al_mt_pressure.STAT
      Alicat Meter temperature OK  :BOOL;        //EPICS Status variable: al_mt_temperature.STAT
      Alicat Meter flow OK  :BOOL;        //EPICS Status variable: al_mt_sccm.STAT
END_VAR
VAR_OUTPUT
      toggle_bit  :BOOL;        //EPICS Command variable: toggle_bit
      al_ctlr_sp  :REAL;        //EPICS Parameter variable: al_ctlr_sp
END_VAR
VAR
	nTempUINT		:UINT;
	sTempHStr		:T_MaxString;

	uREAL2UINTs	:U_REAL_UINTs;
	uUINTs2REAL	:U_REAL_UINTs;
	uTIME2UINTs	:U_TIME_UINTs;
	uUINTs2TIME	:U_TIME_UINTs;
	uDINT2UINTs	:U_DINT_UINTs;
	uUINTs2DINT	:U_DINT_UINTs;
	fValue: INT;
END_VAR

]]>
</Declaration>
<Implementation>
<ST>
<![CDATA[
(*
**********************EPICS<-->Beckhoff integration at ESS in Lund, Sweden*******************************
Data types handler for TCP/IP communication EPICS<--Beckhoff at ESS. Lund, Sweden.
Created by: Andres Quintanilla (andres.quintanilla@esss.se)
            Miklos Boros (miklos.boros@esss.se)
Notes: Converts different types of data into UINT. Adds the converted data into the array to be sent to EPICS.
The first 10 spaces of the array are reserved. nOffset input is used for that. 
Code must not be changed manually. Code is generated and handled by PLC factory at ESS.
Versions:
Version 1: 06/04/2018. Communication stablished and stable
**********************************************************************************************************
*)

    //********************************************
    //*************STATUS VARIABLES***************
    //********************************************


       uREAL2UINTs.fValue :=pressure_01;
       EPICS_GVL.aDataS7[nOffsetStatus + 0]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: pressure_01
       EPICS_GVL.aDataS7[nOffsetStatus + 1]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: pressure_01

       uREAL2UINTs.fValue :=temperature_01;
       EPICS_GVL.aDataS7[nOffsetStatus + 2]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: temperature_01
       EPICS_GVL.aDataS7[nOffsetStatus + 3]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: temperature_01

       EPICS_GVL.aDataS7[nOffsetStatus + 4]           := INT_TO_UINT(al_ctlr_gasindex);       //EPICSName: al_ctlr_gasindex

       uREAL2UINTs.fValue :=al_ctlr_pressure;
       EPICS_GVL.aDataS7[nOffsetStatus + 5]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_ctlr_pressure
       EPICS_GVL.aDataS7[nOffsetStatus + 6]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_ctlr_pressure

       uREAL2UINTs.fValue :=al_ctlr_temperature;
       EPICS_GVL.aDataS7[nOffsetStatus + 7]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_ctlr_temperature
       EPICS_GVL.aDataS7[nOffsetStatus + 8]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_ctlr_temperature

       uREAL2UINTs.fValue :=al_ctlr_ccm;
       EPICS_GVL.aDataS7[nOffsetStatus + 9]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_ctlr_ccm
       EPICS_GVL.aDataS7[nOffsetStatus + 10]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_ctlr_ccm

       uREAL2UINTs.fValue :=al_ctlr_sccm;
       EPICS_GVL.aDataS7[nOffsetStatus + 11]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_ctlr_sccm
       EPICS_GVL.aDataS7[nOffsetStatus + 12]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_ctlr_sccm

       uREAL2UINTs.fValue :=al_ctlr_sp_rbv;
       EPICS_GVL.aDataS7[nOffsetStatus + 13]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_ctlr_sp_rbv
       EPICS_GVL.aDataS7[nOffsetStatus + 14]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_ctlr_sp_rbv

       EPICS_GVL.aDataS7[nOffsetStatus + 15]           := INT_TO_UINT(al_ctlr_ID_rbv);       //EPICSName: al_ctlr_ID_rbv

       EPICS_GVL.aDataS7[nOffsetStatus + 16]           := INT_TO_UINT(al_mt_gasindex);       //EPICSName: al_mt_gasindex

       uREAL2UINTs.fValue :=al_mt_pressure;
       EPICS_GVL.aDataS7[nOffsetStatus + 17]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_mt_pressure
       EPICS_GVL.aDataS7[nOffsetStatus + 18]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_mt_pressure

       uREAL2UINTs.fValue :=al_mt_temperature;
       EPICS_GVL.aDataS7[nOffsetStatus + 19]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_mt_temperature
       EPICS_GVL.aDataS7[nOffsetStatus + 20]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_mt_temperature

       uREAL2UINTs.fValue :=al_mt_ccm;
       EPICS_GVL.aDataS7[nOffsetStatus + 21]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_mt_ccm
       EPICS_GVL.aDataS7[nOffsetStatus + 22]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_mt_ccm

       uREAL2UINTs.fValue :=al_mt_sccm;
       EPICS_GVL.aDataS7[nOffsetStatus + 23]           := uREAL2UINTs.stLowHigh.nLow;       //EPICSName: al_mt_sccm
       EPICS_GVL.aDataS7[nOffsetStatus + 24]           := uREAL2UINTs.stLowHigh.nHigh;       //EPICSName: al_mt_sccm

       EPICS_GVL.aDataS7[nOffsetStatus + 25]           := INT_TO_UINT(al_mt_ID_rbv);       //EPICSName: al_mt_ID_rbv

       nTempUINT.0           := DET-PLCUtil pressure OK;       //EPICSName: pressure_01.STAT
       nTempUINT.1           := DET-PLCUtil temperature OK;       //EPICSName: temperature_01.STAT
       nTempUINT.2           := Alicat Controller pressure OK;       //EPICSName: al_ctlr_pressure.STAT
       nTempUINT.3           := Alicat Controller temperature OK;       //EPICSName: al_ctlr_temperature.STAT
       nTempUINT.4           := Alicat Controller flow OK;       //EPICSName: al_ctlr_sccm.STAT
       nTempUINT.5           := Alicat Meter pressure OK;       //EPICSName: al_mt_pressure.STAT
       nTempUINT.6           := Alicat Meter temperature OK;       //EPICSName: al_mt_temperature.STAT
       nTempUINT.7           := Alicat Meter flow OK;       //EPICSName: al_mt_sccm.STAT
       EPICS_GVL.aDataS7[nOffsetStatus + 26]    := nTempUINT;

    //********************************************
    //*************COMMAND VARIABLES**************
    //********************************************


       nTempUINT			:= EPICS_GVL.aDataModbus[nOffsetCmd + 0];
       toggle_bit             :=     nTempUINT.0;       //EPICSName: toggle_bit
       if (EPICS_GVL.EasyTester <> 2) THEN EPICS_GVL.aDataModbus[nOffsetCmd + 0]:=0; END_IF

    //********************************************
    //************PARAMETER VARIABLES*************
    //********************************************


       uUINTs2REAL.stLowHigh.nLow             := EPICS_GVL.aDataModbus[nOffsetCmd + 1];       //EPICSName: al_ctlr_sp
       uUINTs2REAL.stLowHigh.nHigh             := EPICS_GVL.aDataModbus[nOffsetCmd + 2];       //EPICSName: al_ctlr_sp
       al_ctlr_sp				:= uUINTs2REAL.fValue;

]]>
</ST>
</Implementation>
</POU>
</TcPlcObject>
